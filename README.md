# PPP-device evaluation

I used the following device for a PPP(Precise Point Positioning) device evaluation:

- RaspberryPi 3
- RasPiGNSS navigation module (https://drfasching.com/products/gnss/raspignss.html)
- Tallysman TW-2410 antenna

## installation instructions

1. download and install the latest raspberrian jessie build (http://downloads.raspberrypi.org/raspbian/images/raspbian-2017-07-05/)
2. set a static ip for the raspberry device:
   open the file `/etc/dhcpcd.conf` as sudoers user and set the static ip:<br />
 ```
static value
               interface eth0
               static ip_address=192.168.0.10/24
               static routers=192.168.0.1
               static domain_name_servers=192.168.0.1
```

3. enable and start the ssh-service by following these instruction: https://www.raspberrypi.org/documentation/remote-access/ssh/ <br />
   hint: login-user:pi, default password: raspberry
4. install the navigation module drivers and software by following these instructions (with one expetion, see below): http://drfasching.com/products/gnss/raspignss/installation
   <br/> **important: don't install rpgtools_25_all.deb (some dependencies are broken on yessie), instead use rpgtools-25.tgz.**

## measurement instructions

### measure binary data for ppp postprocessing
You have to use the binr-mode of the navigation module.
1. start navigation module: `sudo ./nvsctl -v init` the output should look like this:
```
rpio I Switching GPIO 22 to output mode
rpio I Enabling pulloff on GPIO 22
rpio I Clearing GPIO 22
rpio I Switching GPIO 23 to output mode
rpio I Enabling pulloff on GPIO 23
rpio I Clearing GPIO 23
rpio I Switching GPIO 24 to output mode
rpio I Enabling pulloff on GPIO 24
rpio I Clearing GPIO 24
rpio I Switching GPIO 18 to input mode
rpio I Enabling pulloff on GPIO 18
rpio I Setting GPIO 18
rpio I Switching GPIO 4 to input mode
rpio I Enabling pulloff on GPIO 4
rpio I Reading GPIO 4
rpio GPIO 4 read as 1
rpio I Switching GPIO 17 to input mode
rpio I Enabling pulloff on GPIO 17
rpio I Reading GPIO 17
rpio GPIO 17 read as 0
rpio I Switching GPIO 25 to input mode
rpio I Enabling pulloff on GPIO 25
rpio I Reading GPIO 25
rpio GPIO 25 read as 0
```

2. set navigation module mode to 'binr': `sudo ./nvsmode -v binr` the output should look like this:
```
rpio I Switching GPIO 18 to input mode
rpio I Enabling pulloff on GPIO 18
rpio I Setting GPIO 18
nvsmode I Switching from NMEA 115200 to BINR 230400
nmeacmd I Sending $PORZA,0,230400,3*7C
speed 230400 baud; rows 0; columns 0; line = 0;
parenb parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
```
3. set binr configuration: `./binrcmd ../etc/init.cmd -v` the output should look like this:
```
binrcmd I Sending 100001210100011003
binrcmd I Sleeping 200 msecs
binrcmd I Sending 10d702021003
binrcmd I Sleeping 200 msecs
binrcmd I Sending 10d70802011003
binrcmd I Sleeping 1000 msecs
binrcmd I Sending 10f4051003
binrcmd I Sleeping 200 msecs
binrcmd I Sending 10d5011003
binrcmd I Sleeping 200 msecs
binrcmd I Sending 100001210100011003
binrcmd I Sleeping 200 msecs
```



4. start recording: `cat /dev/ttyAMA0 > data.nvs`
5. convert to RINEX binary files: `./convbin -r nvs data.nvs`
6. install the rtklib on a remote computer (or use the command line utilites) for ppp postprocessing: (http://www.rtklib.com/)

### measure without ppp correction
you have to use the nmea-mode of the navigation module.
1. start navigation module: `sudo ./nvsctl -v init` the output should look like this:
```
rpio I Switching GPIO 22 to output mode
rpio I Enabling pulloff on GPIO 22
rpio I Clearing GPIO 22
rpio I Switching GPIO 23 to output mode
rpio I Enabling pulloff on GPIO 23
rpio I Clearing GPIO 23
rpio I Switching GPIO 24 to output mode
rpio I Enabling pulloff on GPIO 24
rpio I Clearing GPIO 24
rpio I Switching GPIO 18 to input mode
rpio I Enabling pulloff on GPIO 18
rpio I Setting GPIO 18
rpio I Switching GPIO 4 to input mode
rpio I Enabling pulloff on GPIO 4
rpio I Reading GPIO 4
rpio GPIO 4 read as 1
rpio I Switching GPIO 17 to input mode
rpio I Enabling pulloff on GPIO 17
rpio I Reading GPIO 17
rpio GPIO 17 read as 0
rpio I Switching GPIO 25 to input mode
rpio I Enabling pulloff on GPIO 25
rpio I Reading GPIO 25
rpio GPIO 25 read as 0
```

2. set navigation module to 'nmea' mode: `sudo ./nvsmode -v nmea` the output should look like:
```
rpio I Switching GPIO 18 to input mode
rpio I Enabling pulloff on GPIO 18
rpio I Setting GPIO 18
nvsmode I Switching from BINR 230400 to NMEA 115200
binrcmd I Sending 100b0000c20100021003
speed 115200 baud; rows 0; columns 0; line = 0;
-parenb parodd -cmspar cs8 hupcl -cstopb cread clocal -crtscts
```

4. start recording: `cat /dev/ttyAMA0 > data.mnea`
5. install convert tool: `apt-get install gpsbabel`
6. convert the nmea data to readable csv data: `gpsbabel -t -i nmea -f data.nmea -o unicsv -F output.csv`


## plotting instructions

1. manually modify the position and csv files that the header of the column latitude longitude and altitude are named Latitude Longitude and Altitude
2. start the matlab script https://gitlab.com/tscheims1/ppp-device-evaluation/blob/master/plot-scripts/plot_navigation_points_from_csv.m
3. import the modified position files
4. plot the result