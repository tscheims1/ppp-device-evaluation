# -*- conf -*-
# binrcmd initialization script
# use with "binrcmd -v init.cmd"
# (C) Franz Fasching 2014
# $Id: init.cmd 11 2014-09-26 14:53:03Z ffasching $

# reboot without erasing saved parameters
WARMSTART
WAIT 200

# set navgation rate in Hz (1,2,5,10 Hz)
NAVRATE 2
WAIT 200

# differential correction SBAS w/ RTCA troposphere model
DIFFCOR 2 1
WAIT 1000

# raw data output in intervals of dezi-secs (100ms)
# 1 -> 10Hz, 2 -> 5Hz, 5 -> 2Hz, 10 -> 1Hz (inverse setting but must be the same or greater than NAVRATE !!)
RAWDATA 5
WAIT 200

# bit information transmitted by satellites
BITINF 1
WAIT 200

# reboot without erasing saved parameters
WARMSTART
WAIT 200
