function xyzPos = ToECEFCoordinate(wgs84)
    radius = 6371000;
    
    lat = deg2rad(wgs84(1,1));
    lng = deg2rad(wgs84(1,2));
    alt = wgs84(1,3);
    alt =0;
    
    x = cos(lat)*cos(lng)*(radius+alt);
    y = cos(lat)*sin(lng)*(radius+alt);
    z = sin(lat)*(radius+alt);
    
    xyzPos = [x,y,z];
                
end