clc;


ecefCoords = [];

for i=1:size(Latitude,1)
    ecefCoords(i,:) = ToECEFCoordinate([Latitude(i,:),Longitude(i,:),Altitude(i,:)]);
end
meanWgs84 = mean([Latitude,Longitude,Altitude],1);
meanEcef = ToECEFCoordinate(meanWgs84);
for i=1:size(Latitude,1)
    ecefCoords(i,:) = ecefCoords(i,:)-meanEcef;
end


wgs84Ref = meanWgs84;
ecefRef = meanEcef;


XYCoordinateLocal = []

for i=1:size(Latitude,1)
    XYCoordinateLocal(i,:) =ToENUCoordinate(ecefCoords(i,:),wgs84Ref,ecefRef);
end

std(XYCoordinateLocal)*3

size(XYCoordinateLocal)
plot(XYCoordinateLocal(:,1),XYCoordinateLocal(:,2),'k-','markers',12,'LineWidth',1);
hold on;
plot(XYCoordinateLocal(:,1),XYCoordinateLocal(:,2),'k+','markers',12,'LineWidth',2);

fontsize = 30
xlbl = xlabel('Osten [m]');
set(xlbl,'FontSize',fontsize);
ylbl = ylabel('Norden [m]');
set(gca,'FontSize',fontsize)
set(ylbl,'FontSize',fontsize);

%annotation_35_sbas_moving(gcf,fontsize);

grid on
grid minor
axis equal;

pbaspect([1 1 1])

hold off;






